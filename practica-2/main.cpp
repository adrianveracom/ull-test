#include "vector_t.hpp"
#include "sparse_vector_t.hpp"

#include <iostream>
using namespace std;

#define V_SZ 10000
#define EPS  1E-3

int main(void)
{

  // Original
  // vector_t v(V_SZ);
  //
  // for(int i = 0; i < V_SZ; i++)
  //   v.get_set_v(i) = 0.0;
  //
  // v.get_set_v(0)    = 1.0;
  // v.get_set_v(1000) = 1.0;
  // v.get_set_v(2000) = 1.0;
  // v.get_set_v(3000) = 1.0;
  // v.get_set_v(4000) = 1.0;
  // v.get_set_v(5000) = 1.0;
  //
  // sparse_vector_t sv(v, EPS);
  //
  // sv.write(cout);
  // cout << endl;



  // Leer vector normal
  vector_t v(V_SZ);
  v.read(cin);
  std::cout << std::endl;
  std::cout << "Vector normal" << std::endl;
  v.write(cout);
  cout << endl;
  
  // Leer vector disperso
  sparse_vector_t sv(v, EPS);
  sv.read(cin);
  std::cout << std::endl;
  std::cout << "Vector disperso" << std::endl;
  sv.write(cout);
  cout << endl;

  // Producto escalar de vector normal
  double scalarProductVector = v.scal_prod(v);
  std::cout << std::endl;
  std::cout << "Producto escalar vector normal: " << scalarProductVector << std::endl;

  // Producto escalar de vector disperso con vector normal
  double scalarProductDisperseVector = sv.scal_prod(v);
  std::cout << std::endl;
  std::cout << "Producto escalar vector disperso: " << scalarProductDisperseVector << std::endl;


  // Producto escalar de vector disperso con si mismo
  double scalarProduct = sv.scal_prod(sv);
  std::cout << std::endl;
  std::cout << "Producto escalar vector disperso con si mismo: "<< scalarProduct << std::endl;

  // Escribir vector normal en formato disperso
  std::cout << std::endl;
  std::cout << "Vector disperso en formato denso:" << std::endl;
  sv.write_dense(cout);
  cout << endl;

}

#pragma once

#include "pair_t.hpp"
#include "vector_pair_t.hpp"
#include "vector_t.hpp"

#include <cmath>

#include <iostream>
#include <iomanip>

using namespace std;

class sparse_vector_t{
private:
  vector_pair_t v_;    // Vector de pares
  int           sz_;   // Tamaño del vector original

public:
  sparse_vector_t(void):
    v_(),
    sz_(0) {}

  sparse_vector_t(const vector_t& v, double eps):
    v_(),
    sz_(v.get_sz()) {

    // Cuenta el número de elementos no nulos
    int nz = 0;

    for(int i = 0; i < v.get_sz(); i++)
      if ( is_not_zero(v.get_v(i), eps) )
        nz ++;

    v_.resize(nz);

    nz = 0;

    for(int i = 0; i < v.get_sz(); i++)
      if ( is_not_zero(v.get_v(i), eps) )
      {
        v_.get_set_v(nz).set(i, v.get_v(i));
        nz ++;
      }

  }

  ~sparse_vector_t(void){}

  ostream& write(ostream& os) const{

    os << "[ " << setw(7) << sz_ << " ";
    v_.write(os);
    os << " ]";

    return os;
  }

  istream& read(istream& is){

    is >> sz_;

    // std::cout << "SIZE" << sz_ << std::endl;

    v_.resize(sz_);

    int numeroElementos;
    is >> numeroElementos;

    // std::cout << "numeroElementos" << numeroElementos << std::endl;

    for(int i = 0; i < numeroElementos; i ++) {
      int index;
      double valor;
      is >> index;
      is >> valor;

      pair_t par(index, valor);
      v_.get_set_v(index) = par;
    }

    return is;
  }

  ostream& write_dense(ostream& os) const {
    os << "[ ";
    for (int i = 0; i < sz_; i++) {
      os << v_.get_v(i).get_val() << ",   ";
    }
    os << " ]";
    return os;
  }

  vector_pair_t get_vector() const {
    return v_;
  }

  double scal_prod(const vector_t& v) const{
    double producto_escalar = 0;
    for (int i = 0; i < sz_; i++) {
      producto_escalar = producto_escalar + v_.get_v(i).get_val() * v.get_v(i);
    }
    return producto_escalar;
  }

  double scal_prod(const sparse_vector_t& v) const {
    double total = 0;
    for (int i = 0; i < sz_; i++) {
      total += v_.get_v(i).get_val() * v_.get_v(i).get_val();
    }
    return total;
  }

  // double scal_prod(const sparse_vector_t& v) const {
  //   double producto_escalar = 0;
  //   for (int i = 0; i < v.get_vector().get_sz(); i++) {
  //     producto_escalar = producto_escalar + v_.get_v(i).get_val() * v.get_vector().get_v(i).get_val();
  //   }
  //   return producto_escalar;
  // }


private:
  bool is_not_zero(double val, double eps) { return fabs(val) > eps;}

};

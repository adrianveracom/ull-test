#pragma once

#include "template_matrix_t.hpp"
#include "vector_pair_t.hpp"
#include <cmath>

#include <iostream>
#include <iomanip>

using namespace std;

typedef matrix_t<double>  matrix_double_t;

enum config{COL_CONF=0, ROW_CONF=1};

class sparse_matrix_t
{

  private:
    int m_;  // Numero de filas
    int n_;  // Numero de columnas
    config conf_;
    vector_pair_vp_t Matriz_;

    bool esCero(double a, double eps){
      return (fabs(a) < eps);
    }

  public:
    sparse_matrix_t(void):
      m_(0),
      n_(0),
      conf_(COL_CONF)
    {

    }

    sparse_matrix_t(const matrix_double_t& M, const double eps, const config conf):
      m_(M.get_m()),
      n_(M.get_n()),
      conf_(conf)
    {

      if (conf == ROW_CONF) {

        // 1. Objetivo. Redimensionar Matriz_ con el numero de filas no nulas.

        // Contar el numero de filas que tienen valores no nulos
        // Recorro las filas y columnas de M para encontrarlo
        int filas_no_nulas = 0;

        for (int i = 1; i <= m_; i++) {

          int j = 1;
          bool hay_no_nulos = false;

          while (j <= n_ && !hay_no_nulos) {
            if (!esCero(M.get(i, j), eps)) {
              hay_no_nulos = true;
            }
            j++;
          }

          if (hay_no_nulos == true) {
            filas_no_nulas++;
          }
        }

        std::cout << "FILAS NO NULAS::" << filas_no_nulas << std::endl;
        Matriz_.resize(filas_no_nulas);


        // 2. Objetivo. Remimensionar cada fila con el numero de columnas no nulas

        int k = 0; // Numero de filas con valor no nulos

        for (int i = 1; i <= m_; i++) {
          // Comprobar que hay algun elemento que no es nulo en la fila
          int elementos_no_nulos = 0;
          for (int j = 1; j <= n_; j++) {
            if (!esCero(M.get(i, j), eps)) {
              elementos_no_nulos++;
            }
          }

          // Si hay alguno no nulo
          if (elementos_no_nulos > 0) {
            Matriz_.get_set_v(k).get_val().resize(elementos_no_nulos); // Pongo el tamaño
            Matriz_.get_set_v(k).get_inx() = i; /// Pongo el indice
            std::cout << "COLUMNA NO NULAS EN POS::" << i << "  " <<  elementos_no_nulos << std::endl;

            k++;
          }
        }


        // 3. Objetivo. Rellenar valores en Matriz_
        k = 0; // Numero de filas con valor no nulos
        for (int i = 1; i <= m_; i++) {

          int elementos_no_nulos = 0; // Num

          for (int j = 1; j <= n_; j++) {

            if (!esCero(M.get(i, j), eps)) {
              Matriz_.get_set_v(k).get_val().get_set_v(elementos_no_nulos).set(j, M.get(i, j));
              elementos_no_nulos++;
            }

          }

          // Si hay alguno no nulo
          if (elementos_no_nulos > 0) {
            //Matriz_.get_set_v(k).get_inx() = i; // Pongo el indice
            k++;
          }
        }
      }
      else {  // SI ES COL_CONF

        // Es igual que el de arriba pero:
        // Accedo a M.get(j,i) en vez de M.get(i,j) y..
        m_ = M.get_n();
        n_ = M.get_m();

        // 1. Objetivo. Redimensionar Matriz_ con el numero de filas no nulas.
        // Contar el numero de filas que tienen valores no nulos
        // Recorro las filas y columnas de M para encontrarlo
        int filas_no_nulas = 0;

        for (int i = 1; i <= m_; i++) {

          int j = 1;
          bool hay_no_nulos = false;

          while (j <= n_ && !hay_no_nulos) {
            if (!esCero(M.get(j, i), eps)) {
              hay_no_nulos = true;
            }
            j++;
          }

          if (hay_no_nulos == true) {
            filas_no_nulas++;
          }
        }

        std::cout << "FILAS NO NULAS::" << filas_no_nulas << std::endl;
        Matriz_.resize(filas_no_nulas);


        // 2. Objetivo. Remimensionar cada fila con el numero de columnas no nulas

        int k = 0; // Numero de filas con valor no nulos

        for (int i = 1; i <= m_; i++) {
          // Comprobar que hay algun elemento que no es nulo en la fila
          int elementos_no_nulos = 0;
          for (int j = 1; j <= n_; j++) {
            if (!esCero(M.get(j, i), eps)) {
              elementos_no_nulos++;
            }
          }

          // Si hay alguno no nulo
          if (elementos_no_nulos > 0) {
            Matriz_.get_set_v(k).get_val().resize(elementos_no_nulos); // Pongo el tamaño
            Matriz_.get_set_v(k).get_inx() = i; /// Pongo el indice
            std::cout << "COLUMNA NO NULAS EN POS::" << i << "  " <<  elementos_no_nulos << std::endl;

            k++;
          }
        }


        // 3. Objetivo. Rellenar valores en Matriz_
        k = 0; // Numero de filas con valor no nulos
        for (int i = 1; i <= m_; i++) {

          int elementos_no_nulos = 0; // Num

          for (int j = 1; j <= n_; j++) {

            if (!esCero(M.get(j, i), eps)) {
              Matriz_.get_set_v(k).get_val().get_set_v(elementos_no_nulos).set(j, M.get(j, i));
              elementos_no_nulos++;
            }

          }

          // Si hay alguno no nulo
          if (elementos_no_nulos > 0) {
            k++;
          }
        }
      }

      std::cout << "END" << std::endl;

    }


    ostream& write(ostream& os) const{
      Matriz_.write(os);
      return os;
    }

};

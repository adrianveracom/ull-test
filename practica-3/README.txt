# Practica #3
## Matrices dispersas

### Palabras clave
Vectores dispersos, matrices dispersas, comparación de números reales, constructor por defecto, constructor de copia, referencia, referencia constante, métodos para objetos constantes, lectura y escritura mediante streams.

### Objetivo
Esta práctica persigue que el estudiante aborde nuevamente el concepto de vector disperso, pero esta vez como elemento de una clase más compleja. Para ello debe afianzarse los conceptos estudiados en las prácticas anteriores. Se pretende además que el alumnado sea capaz de componer desde cero una clase compleja a partir de código desarrollado previamente contenido en bibliotecas.

### Material
El alumnado deberá utilizar el material proporcionado en este enunciado:

pair_t.hpp
vector_pair_t.hpp
vector_t.hpp
template_matrix_t.hpp
El fichero pair_t.hpp define una plantilla que describe un par compuesto por un índice y un valor. El tipo de este último valor está parametrizado. El ficherovector_t.hpp contiene una plantilla que describe un vector en el que el tipo de dato que se almacena está parametrizado. El fichero vector_pair_t.hpp contiene las concreciones de dos vectores de tipo vector_t. La primera de las concreciones se refiere a un vector de pares en el que el valor almacenado es un double, y la segunda concreción se refiere a un vector en el que el elemento almacenado es un vector de pares en el que el valor del par es un double. Finalmente el ficherotemplate_matrix_t.hpp contiene una plantilla que representa una matriz de elementos cuyos tipos de dato están parametrizados.

### Desarrollo

#### Fase I
El alumno debe leer y comprender el material proporcionado, estudiando las clases tipo plantilla, comprobando cómo puede llevarse a cabo una concreción de una plantilla, y finalmente estudiando en profundidad la sintaxis de los ficheros.

#### Fase II
El alumno debe representar una matriz dispersa, para ello debe construir una clase que se denominará sparse_matrix_v_t, que contendrá los siguientes atributos:

- Número de filas de la matriz original (m_),
- Número de columnas de la matriz original (n_),
- Una estructura que almacenará los elementos no nulos de la matriz. Esta matriz será un vector cuyos elementos son pares en los que el valor es a su vez un vector de pares donde su valor es un double (M_). La matriz dispersa admite dos configuraciones: columnas y filas. En caso de que la configuración sea de columnas en este vector habrá una entrada por cada columna que tenga al menos un elemento no nulo. Dicha entrada constará de un índice que indica el número de dicha columna, y un valor, que es un vector de pares. Los elementos de este vector de pares son índices indicando la fila del elemento no nulo, el valor de dicho elemento no nulo. En el caso de que la configuración sea de filas se extrapolará la configuración anterior para este caso.
- Un entero con un valor enumerativo (conf_) que contendrá los valores COL_CONF o ROW_CONF, según sea la configuración por columnas o filas, respectivamente.

La clase debe contener como métodos:
- Un constructor al que se le pase una referencia constante de una matriz cuyos elementos sean de tipo double, y un entero con el valor de la configuración de almacenamiento, y un umbral ϵϵ (los detalles sobre cómo llevar a cabo este constructor se darán durante la tutoría),
- Un destructor.
- Un procedimiento de impresión con un parámetro de tipo referencia a ostream, que envíe al ostream el contenido de la matriz,

#### Fase III
Adicionalmente, debe llevarse a cabo un método para la multiplicación de la matriz dispersa invocante por una matriz densa que se pasa como referencia constante, y el resultado, que será una matriz densa, debe almacenarse sobre un parámetro que se pasa como referencia.
